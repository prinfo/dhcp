/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#include "dhcpdatabase.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

DhcpDatabase::DhcpDatabase(QObject *parent) : QObject(parent)
{
}

bool DhcpDatabase::connectToDatabase(const QMap<QString, QString> &connectionInfo)
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName(connectionInfo.value("BackendAddress"));
    db.setDatabaseName(connectionInfo.value("DatabaseName"));
    db.setUserName(connectionInfo.value("DatabaseUsername"));
    db.setPassword(connectionInfo.value("DatabasePassword"));
    if(!db.open()) {
        qCritical() << db.lastError().text();
        return false;
    }

    return true;
}

void DhcpDatabase::closeDatabase()
{
    QSqlDatabase::database().close();
    QString name = QSqlDatabase::database().connectionName();
    QSqlDatabase::removeDatabase(name);
}

QList<DhcpNetwork*> DhcpDatabase::networkQuery()
{
    QList<DhcpNetwork*> networkList;
    QSqlQuery query;

    if(!query.exec("SELECT id, network, relayagent FROM dhcp4_networks")) {
        throw  query.lastError().text();
    }

    while(query.next()) {
        DhcpNetwork* network = new DhcpNetwork(this);
        quint32 networkId = query.value("id").toUInt();
        QString sharedNetName = query.value("network").toString();
        QString relayAgent = query.value("relayagent").toString();
        network->id = networkId;
        network->name = sharedNetName;
        network->relayAgent = relayAgent;
        try {
            network->subnets = subnetQuery(networkId);

        } catch (QString &error) {
            throw error;
        }

        networkList.append(network);
    }

    return networkList;
}

QList<DhcpSubnet*> DhcpDatabase::subnetQuery(const quint32 &networkId)
{
    QList<DhcpSubnet*> subnetsList;
    QSqlQuery query;
    query.prepare("SELECT id, subnet, allowunknown, isstatic FROM dhcp4_subnets WHERE network = ?");
    query.addBindValue(networkId);

    if(!query.exec()) {
        throw  query.lastError().text();
    }

    while(query.next()) {
        quint32 subnetId = query.value("id").toUInt();
        QString net = query.value("subnet").toString();
        bool allowUnknown = query.value("allowunknown").toBool();
        bool isStatic = query.value("isstatic").toBool();

        //Query to check if the subnet contains or is contained within another subnet.
        QSqlQuery subnetCheckQuery;
        subnetCheckQuery.prepare("SELECT subnet FROM dhcp4_subnets WHERE ? >> subnet OR ? << subnet");
        subnetCheckQuery.addBindValue(net);
        subnetCheckQuery.addBindValue(net);
        if(!subnetCheckQuery.exec()) {
            throw subnetCheckQuery.lastError().text();

        } else if(subnetCheckQuery.numRowsAffected() > 0) {
            throw QString("The subnet " + net + " contains or is contained within another subnet");
        }

        QList<QHostAddress>* pool = nullptr;
        try {
            /*poolQuery() will throw a QString error if the query fails, or
            a bool if no pool has been configured for the subnet.*/
            pool = poolQuery(net);

        } catch (QString &error) {
            throw error;

        } catch (bool) {
            throw QString("Pool is empty or out of scope for subnet " + net);
        }

        DhcpSubnet* subnet = new DhcpSubnet(this);
        subnet->id = subnetId;
        subnet->subnet = net;
        subnet->allowUnknown = allowUnknown;
        subnet->isStatic = isStatic;
        subnet->pool = pool;
        subnet->options = getOptions(subnetId);
        subnetsList.append(subnet);
    }

    return subnetsList;
}

QList<QHostAddress>* DhcpDatabase::poolQuery(const QString &subnet)
{
    QSqlQuery query;
    query.prepare("SELECT poolstart, poolend FROM dhcp4_pools WHERE "
                  "poolstart << (SELECT subnet FROM dhcp4_subnets WHERE subnet = ?) AND poolend << (SELECT subnet FROM dhcp4_subnets WHERE subnet = ?)");
    query.addBindValue(subnet);
    query.addBindValue(subnet);

    if(!query.exec()) {
        throw query.lastError().text();
    }

    if(query.numRowsAffected() == 0) {
        throw false;
    }

    //Memory will be freed when the DhcpSubnet class destroys.
    QList<QHostAddress>* pool = new QList<QHostAddress>;
    while(query.next()) {
        QHostAddress startIp(query.value("poolstart").toString());
        QHostAddress endIp(query.value("poolend").toString());

        for(quint32 ip = startIp.toIPv4Address(); ip <= endIp.toIPv4Address(); ++ip) {
            pool->append(QHostAddress(ip));
        }
    }

    return pool;
}

/*Called from DhcpServer every 5 sec to clear leases older than twice their lease time.
It will also check if the database connection is open and try to reconnect if it's not*/
void DhcpDatabase::clearOldLeases()
{
    //isOpen() is trying to reconnect if false.
    if(!QSqlDatabase::database().isOpen()) {
        return;
    }

    QSqlQuery query;
    if(!query.exec("DELETE FROM dhcp4_leases WHERE leasetime < current_timestamp - 2 * "
                   "(SELECT value FROM dhcp4_pooloptions WHERE option = 51 AND subnet = "
                   "dhcp4_leases.subnet)::INTERVAL RETURNING ipaddress, subnet")) {
        qCritical() << query.lastError().text();
        return;
    }

    while(query.next()) {
        quint32 subnetId = query.value("subnet").toUInt();
        QHostAddress leasedIp(query.value("ipaddress").toString());

        //Emit signal to DhcpServer to restore leases to subnet pool.
        emit leaseRemoved(subnetId, leasedIp);
    }
}

/*Called from DhcpServer at start to find existing leases
to clear ips from pools*/
void DhcpDatabase::getExistingLeases()
{
    QSqlQuery query;
    if(!query.exec("SELECT ipaddress, subnet FROM dhcp4_leases")) {
        throw query.lastError().text();
    }

    while(query.next()) {
        quint32 subnetId = query.value("subnet").toUInt();
        QHostAddress ip(query.value("ipaddress").toString());

        //Emit signal to DhcpServer to remove ips to pools.
        emit leasedIp(subnetId, ip);
    }
}

/*Called from DhcpServer at start to remove leases
out of range if a pool has changed.*/
void DhcpDatabase::removeLeasesOutOfScope()
{
    QSqlQuery query;
    if(!query.exec("DELETE FROM dhcp4_leases where ipaddress IN "
                  "(SELECT ipaddress FROM dhcp4_leases JOIN dhcp4_pools ON "
                  "dhcp4_leases.subnet = dhcp4_pools.subnet WHERE "
                  "dhcp4_leases.ipaddress < dhcp4_pools.poolstart OR "
                  "dhcp4_leases.ipaddress > dhcp4_pools.poolend)"))
    {
        throw  query.lastError().text();
    }
}

void DhcpDatabase::removeLease(const QHostAddress &leasedIp, const QByteArray &macAddress)
{
    QSqlQuery query;
    query.prepare("DELETE FROM dhcp4_leases WHERE ipaddress = ? AND "
                  "macaddress = ? RETURNING ipaddress, subnet");
    query.addBindValue(leasedIp.toString());
    query.addBindValue(QString(macAddress.toHex()));
    if(!query.exec()) {
        throw query.lastError().text();
    }

    while(query.next()) {
        quint32 subnetId = query.value("subnet").toUInt();
        QHostAddress leasedIp(query.value("ipaddress").toString());

        //Emit signal to DhcpServer to restore leases to subnet pool.
        emit leaseRemoved(subnetId, leasedIp);
    }
}

QByteArray DhcpDatabase::getOptions(const quint32 &subnetId)
{
    DhcpOption subnetOptions;
    QSqlQuery query;
    query.prepare("SELECT option, value FROM dhcp4_pooloptions WHERE subnet = ?");
    query.addBindValue(subnetId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return subnetOptions.options;
    }

    //Look for vendor options separately
    subnetOptions.append(getVendorOptions(subnetId));

    while(query.next()) {
        char dhcpOption = query.value("option").toInt();

        //Skip options 43, we've already added them.
        if(dhcpOption == 43) {
            continue;
        }

        QString optionValue = query.value("value").toString();
        subnetOptions.append(dhcpOption, optionValue);
    }

    return subnetOptions.options;
}

QByteArray DhcpDatabase::getClientOptions(const quint32 &clientId)
{
    DhcpOption clientOptions;
    QSqlQuery query;
    query.prepare("SELECT option, value FROM dhcp4_clientoptions WHERE client = ?");
    query.addBindValue(clientId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return clientOptions.options;
    }

    //Look for vendor options separately
    clientOptions.append(getClientVendorOptions(clientId));

    while(query.next()) {
        char dhcpOption = query.value("option").toInt();

        //Skip options 43, we've already added them.
        if(dhcpOption == 43) {
            continue;
        }

        QString optionValue = query.value("value").toString();
        clientOptions.append(dhcpOption, optionValue);
    }

    return clientOptions.options;
}

QByteArray DhcpDatabase::getVendorOptions(const quint32 &subnetId)
{
    QByteArray vendorOptions;
    QSqlQuery query;
    query.prepare("SELECT suboption, value, type FROM dhcp4_vendoroptions WHERE name IN "
                  "(SELECT value FROM dhcp4_pooloptions WHERE option = 43 AND subnet = ?)");
    query.addBindValue(subnetId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return vendorOptions;
    }

    //Mainoption
    vendorOptions.append(1, 43);

    //Suboptions
    while(query.next()) {
        char subOption = query.value("suboption").toInt();
        vendorOptions.append(1, subOption);

        int type = query.value("type").toInt();
        switch(type) {
        case Type::String:
            vendorOptions.append(1, query.value("value").toByteArray().size());
            vendorOptions.append(query.value("value").toByteArray());
            break;
        case Type::Uint8:
            vendorOptions.append(1, 1);
            vendorOptions.append(query.value("value").toInt());
            break;
        case Type::Uint16:
            vendorOptions.append(1, 2);
            vendorOptions.append(DhcpOption::int16StringToByteArray(query.value("value").toString()));
            break;
        case Type::Uint32:
            vendorOptions.append(1, 4);
            vendorOptions.append(DhcpOption::int32StringToByteArray(query.value("value").toString()));
            break;
        case Type::IPAddress:
            vendorOptions.append(1, 4);
            vendorOptions.append(DhcpOption::ipStringToByteArray(query.value("value").toString()));
            break;
        case Type::Boolean:
            vendorOptions.append(1, 1);
            vendorOptions.append(1, query.value("value").toBool());
            break;
        case Type::Hex:
            vendorOptions.append(1, QByteArray::fromHex(query.value("value").toByteArray()).size());
            vendorOptions.append(QByteArray::fromHex(query.value("value").toByteArray()));
            break;
        default:
            break;
        }
    }

    //Insert the total size of the suboptions after the mainoption.
    vendorOptions.insert(1, vendorOptions.size()-1);

    return vendorOptions;
}

QByteArray DhcpDatabase::getClientVendorOptions(const quint32 &clientId)
{
    QByteArray vendorOptions;
    QSqlQuery query;
    query.prepare("SELECT suboption, value, type FROM dhcp4_vendoroptions WHERE name IN "
                  "(SELECT value FROM dhcp4_clientoptions WHERE option = 43 AND client = ?)");
    query.addBindValue(clientId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return vendorOptions;
    }

    //Mainoption
    vendorOptions.append(1, 43);

    //Suboptions
    while(query.next()) {
        char subOption = query.value("suboption").toInt();
        vendorOptions.append(1, subOption);

        int type = query.value("type").toInt();
        switch(type) {
        case Type::String:
            vendorOptions.append(1, query.value("value").toByteArray().size());
            vendorOptions.append(query.value("value").toByteArray());
            break;
        case Type::Uint8:
            vendorOptions.append(1, 1);
            vendorOptions.append(query.value("value").toInt());
            break;
        case Type::Uint16:
            vendorOptions.append(1, 2);
            vendorOptions.append(DhcpOption::int16StringToByteArray(query.value("value").toString()));
            break;
        case Type::Uint32:
            vendorOptions.append(1, 4);
            vendorOptions.append(DhcpOption::int32StringToByteArray(query.value("value").toString()));
            break;
        case Type::IPAddress:
            vendorOptions.append(1, 4);
            vendorOptions.append(DhcpOption::ipStringToByteArray(query.value("value").toString()));
            break;
        case Type::Boolean:
            vendorOptions.append(1, 1);
            vendorOptions.append(1, query.value("value").toBool());
            break;
        case Type::Hex:
            vendorOptions.append(1, QByteArray::fromHex(query.value("value").toByteArray()).size());
            vendorOptions.append(QByteArray::fromHex(query.value("value").toByteArray()));
            break;
        default:
            break;
        }
    }

    //Insert the total size of the suboptions after the mainoption.
    vendorOptions.insert(1, vendorOptions.size()-1);

    return vendorOptions;
}

bool DhcpDatabase::addLease(const QHostAddress &ipAddress, const QByteArray &macAddress,
                            const QString &option82, const quint32 &subnetId)
{
    QSqlQuery query;
    query.prepare("INSERT INTO dhcp4_leases (ipaddress, macaddress, option82, subnet)"
                  " values (?, ?, ?, ?)");
    query.addBindValue(ipAddress.toString());
    query.addBindValue(QString(macAddress.toHex()));
    query.addBindValue(option82);
    query.addBindValue(subnetId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return false;
    }

    return true;
}

bool DhcpDatabase::addLease(const QHostAddress &ipAddress, const QByteArray &macAddress,
                            const quint32 &subnetId)
{
    QSqlQuery query;
    query.prepare("INSERT INTO dhcp4_leases (ipaddress, macaddress, subnet) values (?, ?, ?)");
    query.addBindValue(ipAddress.toString());
    query.addBindValue(QString(macAddress.toHex()));
    query.addBindValue(subnetId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return false;
    }

    return true;
}

quint32 DhcpDatabase::clientIpCount(const QString &option82)
{
    QSqlQuery query;
    query.prepare("SELECT ipcount FROM dhcp4_knownclients WHERE value = ?");
    query.addBindValue(option82);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return -1;
    }

    /*If the known client's value is a mac we don't
     need to restrict the count*/
    quint32 ipcount = 2147483647;
    while(query.next()) {
        ipcount = query.value("ipcount").toInt();
    }

    return  ipcount;
}

quint32 DhcpDatabase::clientLeases(const QString &option82, const quint32 &networkId)
{
    quint32 activeLeases = 0;
    QSqlQuery query;
    query.prepare("SELECT count(*) FROM dhcp4_leases JOIN dhcp4_subnets ON "
                  "dhcp4_leases.subnet = dhcp4_subnets.id JOIN dhcp4_networks ON "
                  "dhcp4_subnets.network = dhcp4_networks.id WHERE option82 = ?"
                  "AND dhcp4_networks.id = ?");
    query.addBindValue(option82);
    query.addBindValue(networkId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return -1;
    }

    while(query.next()) {
        activeLeases = query.value("count").toInt();
    }

    return  activeLeases;
}

QHostAddress DhcpDatabase::haveLease(const QByteArray &macAddress, const quint32 &subnetId)
{
    QHostAddress lease;
    QSqlQuery query;
    query.prepare("SELECT ipaddress FROM dhcp4_leases WHERE macaddress = ?"
                  "AND subnet = ?");
    query.addBindValue(QString(macAddress.toHex()));
    query.addBindValue(subnetId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return lease;
    }

    while(query.next()) {
        lease = QHostAddress(query.value("ipaddress").toString());
    }

    return  lease;
}

QHostAddress DhcpDatabase::haveLease(const QByteArray &macAddress)
{
    QHostAddress lease;
    QSqlQuery query;
    query.prepare("SELECT ipaddress FROM dhcp4_leases WHERE macaddress = ?");
    query.addBindValue(QString(macAddress.toHex()));
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return lease;
    }

    while(query.next()) {
        lease = QHostAddress(query.value("ipaddress").toString());
    }

    return lease;
}

bool DhcpDatabase::updateLease(const QByteArray &macAddress, const QHostAddress &ipAddress)
{
    QSqlQuery query;
    query.prepare("UPDATE dhcp4_leases SET leasetime = current_timestamp"
                  " WHERE macaddress = ? AND ipaddress = ?");
    query.addBindValue(QString(macAddress.toHex()));
    query.addBindValue(ipAddress.toString());
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return false;
    }

    return true;
}

quint32 DhcpDatabase::knownClientsQuery(const QByteArray &macAddress, const QString &option82, const quint32 networkId)
{
    QSqlQuery query;
    query.prepare("SELECT id FROM dhcp4_knownclients WHERE (value = ? OR value = ?) AND network = ?");
    query.addBindValue(QString(macAddress.toHex(':')));
    query.addBindValue(option82);
    query.addBindValue(networkId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return 0;
    }

    quint32 clientId = 0;
    while(query.next()) {
        clientId = query.value("id").toInt();
    }

    return clientId;
}

quint32 DhcpDatabase::networkIdFromCiaddr(const QHostAddress &ciAddr)
{
    quint32 networkId = 0;
    QSqlQuery query;
    query.prepare("SELECT network FROM dhcp4_subnets WHERE ? << subnet");
    query.addBindValue(ciAddr.toString());
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return networkId;
    }

    while(query.next()) {
        networkId = query.value("network").toUInt();
    }

    return networkId;
}

QHostAddress DhcpDatabase::staticIpQuery(const quint32 & clientId)
{
    QHostAddress staticIp;
    QSqlQuery query;
    query.prepare("SELECT ipaddress FROM dhcp4_staticips WHERE client = ?");
    query.addBindValue(clientId);
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return staticIp;
    }

    while(query.next()) {
        staticIp = QHostAddress(query.value("ipaddress").toString());
    }

    return staticIp;
}

bool DhcpDatabase::isFreeLease(const QHostAddress &ipAddress)
{
    QSqlQuery query;
    query.prepare("SELECT exists(SELECT 1 FROM dhcp4_leases WHERE ipaddress = ?)");
    query.addBindValue(ipAddress.toString());
    if(!query.exec()) {
        qCritical() << query.lastError().text();
        return false;
    }

    while(query.next()) {
        if(query.value("exists").toBool()) {
            return false;
        }
    }

    return true;
}
