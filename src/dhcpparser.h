/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#ifndef PACKETPARSER_H
#define PACKETPARSER_H

#include <QObject>
#include <QHostAddress>

class DhcpParser : public QObject
{
    Q_OBJECT
public:
    explicit DhcpParser(const QByteArray &dhcpPacket, QObject *parent = nullptr);
    QByteArray op;
    QByteArray hType;
    QByteArray hLen;
    QByteArray hops;
    QByteArray xId;
    QByteArray secs;
    QByteArray flags;
    QByteArray ciaddr;
    QByteArray yiaddr;
    QByteArray siaddr;
    QByteArray giaddr;
    QByteArray chaddr;
    QByteArray sname;
    QByteArray bootFile;
    QByteArray magicCookie;
    QByteArray dhcpOptions;
    QByteArray circuitId;
    QByteArray remoteId;
    QByteArray vendorId;
    QByteArray messageType;

    QByteArray getOptionData(const quint8 &option) const;
    QByteArray getCircuitID();
    QByteArray getRemoteID();

};

#endif // PACKETPARSER_H
