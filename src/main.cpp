/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QFile>
#include <QJsonObject>
#include <QJsonParseError>
#include "dhcpserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //Console arguments
    //--version
    QCoreApplication::setApplicationName("Tiat");
    QCoreApplication::setApplicationVersion("1.0.1");

    QCommandLineParser cParser;
    cParser.addHelpOption();
    cParser.addVersionOption();

    //--test
    QCommandLineOption testConfig(QStringList() <<  "t" << "test", "Test the configuration");
    cParser.addOption(testConfig);

    cParser.process(a);

    QFile file("/etc/tiat/setup.conf");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qCritical() << "Could not open" << file.fileName();
        return EXIT_FAILURE;
    }

    QByteArray config = file.readAll();
    file.close();

    QJsonParseError jError;
    QJsonDocument jDoc = QJsonDocument::fromJson(config, &jError);
    if(jError.error != QJsonParseError::NoError) {
        qCritical() << jError.errorString();
        return EXIT_FAILURE;
    }

    QJsonObject obj = jDoc.object();
    QJsonObject dhcp = obj.value("dhcp").toObject();
    QJsonObject backend = obj.value("backend").toObject();
    QJsonObject hapair = obj.value("hapair").toObject();

    QMap<QString, QString> connectionInfo;
    connectionInfo.insert("ServerAddress", dhcp.value("address").toString());
    connectionInfo.insert("BackendAddress", backend.value("address").toString());
    connectionInfo.insert("DatabaseName", backend.value("database").toString());
    connectionInfo.insert("DatabaseUsername", backend.value("username").toString());
    for(int i = 0; i < 4; ++i) {
        if(connectionInfo.values().value(i).isEmpty() ) {
            qCritical() << connectionInfo.key(connectionInfo.values().value(i)) << "in setup.conf is not set";
            return EXIT_FAILURE;
        }
    }

    //Password and hapair are allowed to be empty
    connectionInfo.insert("DatabasePassword", backend.value("password").toString());
    connectionInfo.insert("HaRole", hapair.value("role").toString());
    connectionInfo.insert("HaAddress", hapair.value("address").toString());
    connectionInfo.insert("HaPort", hapair.value("port").toString());

    DhcpServer dhcpServer;
    if(cParser.isSet(testConfig)) {
        dhcpServer.testDhcp(connectionInfo);
        return EXIT_SUCCESS;
    }

    dhcpServer.startDhcp(connectionInfo);

    return a.exec();
}
