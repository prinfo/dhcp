/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#include "tiatha.h"
#include <QProcess>

TiatHA::TiatHA(const QMap<QString, QString> &connectionInfo, QObject *parent)
    : QObject(parent), connectionInfo(connectionInfo)
{
    address = QHostAddress(connectionInfo.value("HaAddress"));
    port = quint16(connectionInfo.value("HaPort").toUInt());
}

void TiatHA::startHaMaster()
{
    if(!master.listen(address, port)) {
        throw QString("Tiat HA: ") + master.errorString();

    } else {
        qInfo() << "Tiat HA Running: Master";
    }
}

void TiatHA::startHaSlave()
{
    slave.connectToHost(address, port);
    if(!slave.waitForConnected(10000)) {
        throw QString("Tiat HA: ") + slave.errorString();

    } else {
        QProcess setKeepAliveOption;
        setKeepAliveOption.start("sysctl -w net.ipv4.tcp_keepalive_time=2");
        if(!setKeepAliveOption.waitForFinished(2000) || setKeepAliveOption.exitCode() != 0) {
            throw QString("Could not set keepalive time");
        }

        setKeepAliveOption.start("sysctl -w net.ipv4.tcp_keepalive_intvl=1");
        if(!setKeepAliveOption.waitForFinished(2000) || setKeepAliveOption.exitCode() != 0) {
            throw QString("Could not set keepalive interval");
        }

        setKeepAliveOption.start("sysctl -w net.ipv4.tcp_keepalive_probes=10");
        if(!setKeepAliveOption.waitForFinished(2000) || setKeepAliveOption.exitCode() != 0) {
            throw QString("Could not set keepalive probes");
        }

        connect(&slave, &QTcpSocket::disconnected, this, &TiatHA::masterConnectionClosed);
        connect(&reconnectTimer, &QTimer::timeout, this, &TiatHA::checkMasterStatus);
        slave.setSocketOption(QAbstractSocket::KeepAliveOption, 1);

        qInfo() << "Tiat HA Running: Slave";
    }
}

void TiatHA::masterConnectionClosed()
{
    qCritical() << "Lost connection to master";
    QTimer::singleShot(1000, this, &TiatHA::reconnectToMaster);
}

void TiatHA::checkMasterStatus()
{
    slave.connectToHost(address, port);
    if(!slave.waitForConnected(1000)) {
        return;

    } else {
        qInfo() << "Reconnected to master: Shutting down DHCP service...";
        reconnectTimer.stop();
        emit stopDhcp();
        slave.setSocketOption(QAbstractSocket::KeepAliveOption, 1);
    }
}

void TiatHA::reconnectToMaster()
{
    qInfo() << "Trying to reconnect...";
    slave.connectToHost(address, port);
    if(!slave.waitForConnected(2000)) {

        //Remove the HA role so it won't start as slave again.
        connectionInfo.remove("HaRole");
        qCritical() << "Failed: Taking over DHCP service...";

        emit startDhcp(connectionInfo);

        reconnectTimer.start(1000);

    } else {
        qInfo() << "Reconnected";
    }
}
