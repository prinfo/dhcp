/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#ifndef DHCPSERVER_H
#define DHCPSERVER_H

#include "dhcpdatabase.h"
#include "dhcpparser.h"
#include "tiatha.h"
#include <QObject>
#include <QUdpSocket>
#include <QTimer>

class DhcpServer : public QObject
{
    Q_OBJECT
    QUdpSocket* dhcpSocket;
    DhcpDatabase* database;
    QList<DhcpNetwork*> networkList;
    QTimer* leaseTimer;
    TiatHA* tiatHa;
    void dhcpOffer(const DhcpParser &dhcpPacket, const DhcpNetwork* network);
    void dhcpAck(const DhcpParser &dhcpPacket, const DhcpNetwork* network);
    void dhcpAckRenewal(const DhcpParser &dhcpPacket, const QHostAddress &sourceAddress);
    void dhcpNak(const DhcpParser &dhcpPacket, const QHostAddress &sourceAddress, const quint16 &port);
    void dhcpRelease(const QHostAddress &leasedIp, const QByteArray &macAddress);
    void padPacket(QByteArray& packet);
    void addClientOptions(QByteArray& poolOptions, const QByteArray& clientOptions);
    QByteArray ipToArray(const QHostAddress &ip);
    QByteArray getOption82(const DhcpParser& dhcpPacket);

    enum DHCPMessage {
        Discover = 1,
        Offer,
        Request,
        Decline,
        Ack,
        Nak,
        Release,
        Inform
    };

public:
    explicit DhcpServer(QObject *parent = nullptr);
    ~DhcpServer();
    QByteArray serverIp;
    void testDhcp(const QMap<QString, QString> &connectionInfo);
    quint32 availabeLeases(const quint32 &subnetId);

private slots:
    void dhcpRecieve();
    void restoreIpToPool(const quint32 &subnetId, const QHostAddress &leasedIp);
    void removeIpFromPool(const quint32 &subnetId, const QHostAddress &leasedIp);
    void stopDhcp();

public slots:
    void startDhcp(const QMap<QString, QString> &connectionInfo);

};

#endif // DHCPSERVER_H
