/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#include "dhcpserver.h"
#include <QDebug>
#include <QNetworkDatagram>

DhcpServer::DhcpServer(QObject *parent) : QObject(parent)
{
}

void DhcpServer::testDhcp(const QMap<QString, QString> &connectionInfo)
{
    DhcpDatabase testDatabase;
    if(testDatabase.connectToDatabase(connectionInfo)) {
        try {
            testDatabase.networkQuery();

        } catch(QString error) {
            qCritical() << error;
            return;
        }

        qInfo() << "Configuration OK";
    }
}

void DhcpServer::startDhcp(const QMap<QString, QString> &connectionInfo)
{
    QString HaRole = connectionInfo.value("HaRole");
    if(!HaRole.isEmpty()) {
        tiatHa = new TiatHA(connectionInfo, this);

        if(HaRole == "master") {
            try {
                tiatHa->startHaMaster();

            } catch (QString error) {
                qCritical() << error;
                exit(EXIT_FAILURE);
            }

        } else if(HaRole == "slave") {
            try {
                tiatHa->startHaSlave();

            } catch (QString error) {
                qCritical() << error;
                exit(EXIT_FAILURE);
            }

            connect(tiatHa, &TiatHA::startDhcp, this, &DhcpServer::startDhcp);
            connect(tiatHa, &TiatHA::stopDhcp, this, &DhcpServer::stopDhcp);

            return;

        } else {
            qCritical() << "Invalid HA Role";
            exit(EXIT_FAILURE);;
        }
    }

    QHostAddress ip = QHostAddress(connectionInfo.value("ServerAddress"));
    this->serverIp = ipToArray(ip);

    database = new DhcpDatabase(this);
    if(!database->connectToDatabase(connectionInfo)) {
        exit(EXIT_FAILURE);
    }

    connect(database, &DhcpDatabase::leaseRemoved, this, &DhcpServer::restoreIpToPool);
    connect(database, &DhcpDatabase::leasedIp, this, &DhcpServer::removeIpFromPool);

    try {
        networkList = database->networkQuery();

        /*If a pool has been made smaller we need to remove the existing
         leases that are out of range.*/
        database->removeLeasesOutOfScope();

        //Get existing leases from database to remove them from subnet pools.
        database->getExistingLeases();

    } catch(QString error) {
        qCritical() << error;
        exit(EXIT_FAILURE);
    }

    dhcpSocket = new QUdpSocket(this);
    if(!dhcpSocket->bind(ip, 67)) {
        qCritical() << dhcpSocket->errorString();
        exit(EXIT_FAILURE);
    }
    connect(dhcpSocket, &QUdpSocket::readyRead, this, &DhcpServer::dhcpRecieve);

    //Timer to clear old leases.
    leaseTimer = new QTimer(this);
    connect(leaseTimer, &QTimer::timeout, database, &DhcpDatabase::clearOldLeases);
    leaseTimer->start(5000);

    qInfo() << "Tiat DHCP Running";
}

DhcpServer::~DhcpServer()
{
}

void DhcpServer::dhcpRecieve()
{
    while(dhcpSocket->hasPendingDatagrams()) {
        QNetworkDatagram datagram = dhcpSocket->receiveDatagram();
        DhcpParser dhcpPacket(datagram.data());
        ushort option53 = dhcpPacket.messageType.toHex().toUShort(Q_NULLPTR, 16);
        QHostAddress giAddr(dhcpPacket.giaddr.toHex().toUInt(Q_NULLPTR, 16));
        QHostAddress ciAddr(dhcpPacket.ciaddr.toHex().toUInt(Q_NULLPTR, 16));
        QHostAddress requestedIp;
        QString option82 = getOption82(dhcpPacket);

        for(int i = 0; i < networkList.count(); ++i) {
            DhcpNetwork* network = networkList.value(i);
            QPair<QHostAddress, int> relayAgent = QHostAddress::parseSubnet(network->relayAgent);
            if(giAddr.isInSubnet(relayAgent)) {
                switch (option53) {
                case DHCPMessage::Discover:
                    qInfo() << "DHCPDiscover from" << option82 <<
                               "(" << dhcpPacket.chaddr.toHex(':') << ")";
                    dhcpOffer(dhcpPacket, network);
                    return;
                case DHCPMessage::Request:
                    if(ciAddr.isEqual(QHostAddress("0.0.0.0"))) {
                        requestedIp = QHostAddress(dhcpPacket.getOptionData(50).toHex().toUInt(Q_NULLPTR, 16));
                    } else {
                        requestedIp = ciAddr;
                    }
                    qInfo() << "DHCPRequest for" << requestedIp.toString() << "from" << option82
                            << "(" << dhcpPacket.chaddr.toHex(':') << ")";
                    dhcpAck(dhcpPacket, network);
                    return;
                case DHCPMessage::Decline:
                    if(ciAddr.isEqual(QHostAddress("0.0.0.0"))) {
                        requestedIp = QHostAddress(dhcpPacket.getOptionData(50).toHex().toUInt(Q_NULLPTR, 16));
                    } else {
                        requestedIp = ciAddr;
                    }
                    qInfo() << "DHCPDecline for" << requestedIp.toString() << "from" << option82
                            << "(" << dhcpPacket.chaddr.toHex(':') << ")";
                    database->removeLease(requestedIp, dhcpPacket.chaddr);
                    return;
                case DHCPMessage::Inform:
                    //Handle DHCPINFORM???
                    return;
                default:
                    return;
                }
            }
        }

        /*If it's a unicast renewal the giAddr is not set but ciAddr is, and the
        server should reply directly to the client*/
        if(option53 == DHCPMessage::Request && !ciAddr.isEqual(QHostAddress("0.0.0.0"))) {
            qInfo() << "DHCPRequest for" << ciAddr.toString() << "from" << option82
                    << "(" << dhcpPacket.chaddr.toHex(':') << ")";
            dhcpAckRenewal(dhcpPacket, datagram.senderAddress());
            return;

        } else if(option53 == DHCPMessage::Release && !ciAddr.isEqual(QHostAddress("0.0.0.0"))) {
            qInfo() << "DHCPRelease from" << option82
                    << "(" << dhcpPacket.chaddr.toHex(':') << ") on" << ciAddr.toString();
            dhcpRelease(ciAddr, dhcpPacket.chaddr);
            return;
        }

        qInfo() << "No matching net for" << option82 << "(" << dhcpPacket.chaddr.toHex(':') << ")";
    }
}

void DhcpServer::dhcpOffer(const DhcpParser &dhcpPacket, const DhcpNetwork* network)
{
    QByteArray offer;
    QByteArray options;
    QByteArray mac = dhcpPacket.chaddr;
    QString option82 = getOption82(dhcpPacket);

    quint32 knownClient = database->knownClientsQuery(mac, option82, network->id);

    QHostAddress clientIp = database->haveLease(mac);
    DhcpSubnet* subnet = nullptr;

    if(!clientIp.isNull()) {
        for(int i = 0; i < network->subnets.count(); ++i) {
            subnet = network->subnets.value(i);
            QPair<QHostAddress, int> cidr = QHostAddress::parseSubnet(subnet->subnet);
            if(clientIp.isInSubnet(cidr)) {
                //If a client is changed between unknown and known.
                if((subnet->allowUnknown && knownClient) || (!subnet->allowUnknown && !knownClient)) {
                    clientIp.clear();
                    break;
                } else {
                    options = subnet->options;
                    break;
                }
            }
        }
    }

    if(clientIp.isNull()) {
        QHostAddress staticIp;
        if(knownClient) {
            staticIp = database->staticIpQuery(knownClient);
        }

        for(int i = 0; i < network->subnets.count(); ++i) {
            subnet = network->subnets.value(i);

            if(subnet->pool->isEmpty()) {
                continue;

            } else if(!subnet->allowUnknown && knownClient) {

                if(!staticIp.isNull() || subnet->isStatic) {
                    QPair<QHostAddress, int> cidr = QHostAddress::parseSubnet(subnet->subnet);
                    if(staticIp.isInSubnet(cidr) && database->isFreeLease(staticIp)) {
                        clientIp = staticIp;
                        database->addLease(clientIp, mac, option82, subnet->id);
                        options = subnet->options;
                        subnet->pool->removeOne(clientIp);
                        break;
                    }

                } else if(database->clientIpCount(option82) > database->clientLeases(option82, network->id)) {
                    clientIp = subnet->pool->takeFirst();
                    database->addLease(clientIp, mac, option82, subnet->id);
                    options = subnet->options;
                    break;
                } else {
                    qInfo() << "Max IP count reached for" << option82;
                    return;
                }

            /*If a client will be offered an IP from an unrestricted subnet
            we don't add the option82 value to the lease database*/
            } else if(subnet->allowUnknown && !knownClient) {
                clientIp = subnet->pool->takeFirst();
                database->addLease(clientIp, mac, subnet->id);
                options = subnet->options;
                break;
            }

            if(i == network->subnets.count()-1) {
                qInfo() << "No available IP in" << network->name
                        << "for" << option82 << "(" << mac.toHex(':') << ")";
                return;
            }
        }
    }

    if(knownClient) {
        QByteArray clientOptions = database->getClientOptions(knownClient);
        //Add clientOptions to options
        addClientOptions(options, clientOptions);
    }

    offer.append(1, 2);     //OP Code
    offer.append(dhcpPacket.hType);
    offer.append(dhcpPacket.hLen);
    offer.append(dhcpPacket.hops);
    offer.append(dhcpPacket.xId);
    offer.append(2, 0);     //Secs
    offer.append(dhcpPacket.flags);
    offer.append(4, 0);     //ciAddr
    offer.append(ipToArray(clientIp));
    offer.append(4, 0);     //siAddr
    offer.append(dhcpPacket.giaddr);
    offer.append(mac);
    offer.append(10, 0);    //chAddr padding
    offer.append(dhcpPacket.sname);
    offer.append(dhcpPacket.bootFile);
    offer.append(dhcpPacket.magicCookie);
    offer.append(1, 53);    //Option 53(DHCP Message Type)
    offer.append(1, 1);     //Message length
    offer.append(1, 2);     //Message (2 = DHCPOFFER)
    offer.append(1, 54);    //Option 54(Server Identifier)
    offer.append(1, 4);     //Message length
    offer.append(serverIp);
    offer.append(options);
    offer.append(1, 255);   //Packet End

    padPacket(offer);

    dhcpSocket->writeDatagram(offer, QHostAddress(dhcpPacket.giaddr.toHex().toUInt(Q_NULLPTR, 16)), 67);
    qInfo() << "DHCPOffer on" << clientIp.toString() << "to" <<
               option82 << "(" << mac.toHex(':') << ")";
}

void DhcpServer::dhcpAck(const DhcpParser &dhcpPacket, const DhcpNetwork* network)
{
    QByteArray ack;
    QByteArray options;
    QByteArray mac = dhcpPacket.chaddr;
    QString option82 = getOption82(dhcpPacket);

    QHostAddress clientIp;
    quint32 knownClient = database->knownClientsQuery(mac, option82, network->id);

    for(int i = 0; i < network->subnets.count(); ++i) {
        DhcpSubnet* subnet = network->subnets.value(i);

        /*Continue to the next subnet if the subnet doesn't allow unknown clients
        and the client is unknown*/
        if((!subnet->allowUnknown && !knownClient)
                || (subnet->allowUnknown && knownClient)) {
            continue;
        }

        clientIp = database->haveLease(mac, subnet->id);
        QPair<QHostAddress, int> cidr = QHostAddress::parseSubnet(subnet->subnet);
        if(clientIp.isInSubnet(cidr)) {

            QHostAddress staticIp = database->staticIpQuery(knownClient);
            if((subnet->isStatic && staticIp.isNull())
                    || (!subnet->isStatic && !staticIp.isNull())) {
                break;
            }

            options = subnet->options;
            database->updateLease(mac, clientIp);

            if(knownClient) {
                QByteArray clientOptions = database->getClientOptions(knownClient);
                //Add clientOptions to options
                addClientOptions(options, clientOptions);
            }

            ack.append(1, 2);   //OP Code
            ack.append(dhcpPacket.hType);
            ack.append(dhcpPacket.hLen);
            ack.append(dhcpPacket.hops);
            ack.append(dhcpPacket.xId);
            ack.append(2, 0);   //Secs
            ack.append(dhcpPacket.flags);
            ack.append(4, 0);   //ciAddr
            ack.append(ipToArray(clientIp));
            ack.append(4, 0);   //siAddr
            ack.append(dhcpPacket.giaddr);
            ack.append(mac);
            ack.append(10, 0);  //ChaAddr padding
            ack.append(dhcpPacket.sname);
            ack.append(dhcpPacket.bootFile);
            ack.append(dhcpPacket.magicCookie);
            ack.append(1, 53);  //Option 53(DHCP Message Type)
            ack.append(1, 1);   //Message length
            ack.append(1, 5);   //Message (5 = DHCPACK)
            ack.append(1, 54);  //Option 54(Server Identifier)
            ack.append(1, 4);   //Message length
            ack.append(serverIp);
            ack.append(options);
            ack.append(1, 255); //Packet End

            padPacket(ack);

            dhcpSocket->writeDatagram(ack, QHostAddress(dhcpPacket.giaddr.toHex().toUInt(Q_NULLPTR, 16)), 67);
            qInfo() << "DHCPAck on" << clientIp.toString() << "to" <<
                        option82 << "(" << mac.toHex(':') << ")";
            return;
        }
    }

    dhcpNak(dhcpPacket, QHostAddress(dhcpPacket.giaddr.toHex().toUInt(Q_NULLPTR, 16)), 67);
}

void DhcpServer::dhcpAckRenewal(const DhcpParser &dhcpPacket, const QHostAddress &sourceAddress)
{
    QByteArray ack;
    QByteArray options;
    QByteArray mac = dhcpPacket.chaddr;
    QString option82 = getOption82(dhcpPacket);

    QHostAddress clientIp(dhcpPacket.ciaddr.toHex().toUInt(Q_NULLPTR, 16));
    quint32 networkId = database->networkIdFromCiaddr(clientIp);
    if(networkId < 1) {
        dhcpNak(dhcpPacket, sourceAddress, 68);
        return;
    }

    DhcpNetwork* network = nullptr;
    for(int i = 0; i < networkList.count(); ++i) {
        if(networkList.value(i)->id == networkId) {
            network = networkList.value(i);
            break;
        }
    }

    quint32 knownClient = database->knownClientsQuery(mac, option82, networkId);

    for(int i = 0; i < network->subnets.count(); ++i) {
        DhcpSubnet* subnet = network->subnets.value(i);

        /*Continue to the next subnet if the subnet doesn't allow unknown clients
        and the client is unknown*/
        if((!subnet->allowUnknown && !knownClient)
                || (subnet->allowUnknown && knownClient)) {
            continue;
        }

        QHostAddress requestedIp = database->haveLease(mac, subnet->id);
        QPair<QHostAddress, int> cidr = QHostAddress::parseSubnet(subnet->subnet);
        if(requestedIp.isInSubnet(cidr)) {

            QHostAddress staticIp = database->staticIpQuery(knownClient);
            if((subnet->isStatic && staticIp.isNull())
                    || (!subnet->isStatic && !staticIp.isNull())) {
                break;
            }

            options = subnet->options;
            database->updateLease(mac, requestedIp);

            if(knownClient) {
                QByteArray clientOptions = database->getClientOptions(knownClient);
                //Add clientOptions to options
                addClientOptions(options, clientOptions);
            }

            ack.append(1, 2);   //OP Code
            ack.append(dhcpPacket.hType);
            ack.append(dhcpPacket.hLen);
            ack.append(dhcpPacket.hops);
            ack.append(dhcpPacket.xId);
            ack.append(2, 0);   //Secs
            ack.append(dhcpPacket.flags);
            ack.append(dhcpPacket.ciaddr); //ciAddr
            ack.append(ipToArray(requestedIp));
            ack.append(4, 0);   //siAddr
            ack.append(4, 0);   //giAddr
            ack.append(mac);
            ack.append(10, 0);  //ChaAddr padding
            ack.append(dhcpPacket.sname);
            ack.append(dhcpPacket.bootFile);
            ack.append(dhcpPacket.magicCookie);
            ack.append(1, 53);  //Option 53(DHCP Message Type)
            ack.append(1, 1);   //Message length
            ack.append(1, 5);   //Message (5 = DHCPACK)
            ack.append(1, 54);  //Option 54(Server Identifier)
            ack.append(1, 4);   //Message length
            ack.append(serverIp);
            ack.append(options);
            ack.append(1, 255); //Packet End

            padPacket(ack);

            dhcpSocket->writeDatagram(ack, sourceAddress, 68);
            qInfo() << "DHCPAck on" << requestedIp.toString() << "to" <<
                        option82 << "(" << mac.toHex(':') << ")";
            return;
        }
    }

    dhcpNak(dhcpPacket, sourceAddress, 68);
}

void DhcpServer::dhcpNak(const DhcpParser &dhcpPacket, const QHostAddress &sourceAddress, const quint16 &port)
{
    QByteArray nak;
    QByteArray options;
    QByteArray mac = dhcpPacket.chaddr;
    QHostAddress ciAddr(dhcpPacket.ciaddr.toHex().toUInt(Q_NULLPTR, 16));
    QHostAddress requestedIp;
    QString option82 = getOption82(dhcpPacket);

    nak.append(1, 2);   //OP Code
    nak.append(dhcpPacket.hType);
    nak.append(dhcpPacket.hLen);
    nak.append(dhcpPacket.hops);
    nak.append(dhcpPacket.xId);
    nak.append(2, 0);   //Secs
    nak.append(dhcpPacket.flags);
    nak.append(4, 0);   //ciAddr
    nak.append(4, 0);   //yiAddr
    nak.append(4, 0);   //siAddr
    nak.append(4, 0);   //giAddr
    nak.append(mac);
    nak.append(10, 0);  //ChaAddr padding
    nak.append(dhcpPacket.sname);
    nak.append(dhcpPacket.bootFile);
    nak.append(dhcpPacket.magicCookie);
    nak.append(1, 53);  //Option 53(DHCP Message Type)
    nak.append(1, 1);   //Message length
    nak.append(1, 6);   //Message (6 = DHCPNAK)
    nak.append(1, 54);  //Option 54(Server Identifier)
    nak.append(1, 4);   //Message length
    nak.append(serverIp);
    nak.append(1, 255); //Packet End

    padPacket(nak);

    if(ciAddr.isEqual(QHostAddress("0.0.0.0"))) {
        requestedIp = QHostAddress(dhcpPacket.getOptionData(50).toHex().toUInt(Q_NULLPTR, 16));
    } else {
        requestedIp = ciAddr;
    }

    try {
        database->removeLease(requestedIp, mac);

    } catch (QString error) {
        qCritical() << error;
        return;
    }

    dhcpSocket->writeDatagram(nak, sourceAddress, port);
    qInfo() << "DHCPNak on" << requestedIp.toString() << "for" << option82 << "(" << mac.toHex(':') << ")";
}

void DhcpServer::dhcpRelease(const QHostAddress &leasedIp, const QByteArray &macAddress)
{
    try {
        database->removeLease(leasedIp, macAddress);

    } catch (QString error) {
        qCritical() << error;
    }

}

void DhcpServer::padPacket(QByteArray &packet)
{
    if(packet.count() < 300) {
        packet.append(300 - packet.count(), 0);
    }
}

/*Called when DhcpDatabase signals that a lease has been removed
from the database and is about to be restored to pool*/
void DhcpServer::restoreIpToPool(const quint32 &subnetId, const QHostAddress &leasedIp)
{
    for(int i = 0; i < networkList.count(); ++i) {
        DhcpNetwork* network = networkList.value(i);
        for(int x = 0; x < network->subnets.count(); ++x) {
            if(network->subnets.value(x)->id == subnetId) {
                network->subnets.value(x)->pool->append(leasedIp);
            }
        }
    }
}

/*Called when DhcpDatabase signals that lease is taken at start
and needs to be removed from pool*/
void DhcpServer::removeIpFromPool(const quint32 &subnetId, const QHostAddress &leasedIp)
{
    for(int i = 0; i < networkList.count(); ++i) {
        DhcpNetwork* network = networkList.value(i);
        for(int x = 0; x < network->subnets.count(); ++x) {
            if(network->subnets.value(x)->id == subnetId) {
                network->subnets.value(x)->pool->removeOne(leasedIp);
            }
        }
    }
}

quint32 DhcpServer::availabeLeases(const quint32 &subnetId)
{
    quint32 availabeLeases = 0;

    for(int i = 0; i < networkList.count(); ++i) {
        DhcpNetwork* network = networkList.value(i);
        for(int x = 0; x < network->subnets.count(); ++x) {
            if(network->subnets.value(x)->id == subnetId) {
                availabeLeases = network->subnets.value(x)->pool->count();
            }
        }
    }

    return availabeLeases;
}

/*If we add QHostAddress to QByteArray we send it as ASCII,
so we split it into chars first and add each char to the array*/
QByteArray DhcpServer::ipToArray(const QHostAddress &ip)
{
    quint32 i = ip.toIPv4Address();
    QVector<uchar> chars;
    chars.append((i >> 24) & 0xFF);
    chars.append((i >> 16) & 0xFF);
    chars.append((i >> 8) & 0xFF);
    chars.append(i & 0xFF);

    QByteArray convertedIp;
    convertedIp.append(chars.value(0));
    convertedIp.append(chars.value(1));
    convertedIp.append(chars.value(2));
    convertedIp.append(chars.value(3));

    return convertedIp;
}

QByteArray DhcpServer::getOption82(const DhcpParser& dhcpPacket)
{
    if(dhcpPacket.remoteId.isEmpty()) {
        return dhcpPacket.circuitId;
    } else {
        return dhcpPacket.circuitId + "_" + dhcpPacket.remoteId;
    }
}

void DhcpServer::addClientOptions(QByteArray& poolOptions, const QByteArray& clientOptions)
{
    for(int i = 0; i < clientOptions.count();) {
        QByteArray option = clientOptions.mid(i, 1);
        ushort cLength = clientOptions.mid(i+1, 1).toHex().toUShort(Q_NULLPTR, 16);
        QByteArray clientOptionData = clientOptions.mid(i+2, cLength);

        for(int y = 0; y < poolOptions.count();) {
            ushort pLength = poolOptions.mid(y+1, 1).toHex().toUShort(Q_NULLPTR, 16);
            if(option == poolOptions.mid(y, 1)) {

                //Edit option43 suboptions
                if(option.toHex().toUShort(Q_NULLPTR, 16) == 43) {
                    QByteArray vendorOptions = poolOptions.mid(y+2, pLength);
                    addClientOptions(vendorOptions, clientOptionData);

                    //Replace size
                    poolOptions.replace(y+1, 1, QByteArray(1, vendorOptions.size()));
                    //Replace data
                    poolOptions.replace(y+2, pLength, vendorOptions);
                    break;

                } else {
                    //Replace size
                    poolOptions.replace(y+1, 1, clientOptions.mid(i+1, 1));
                    //Replace data
                    poolOptions.replace(y+2, pLength, clientOptionData);
                    break;
                }
            }

            y += pLength + 2;

            if(y == poolOptions.count()) {
                poolOptions.append(option);
                poolOptions.append(1 , cLength);
                poolOptions.append(clientOptionData);
            }
        }

        i += cLength + 2;
    }
}

void DhcpServer::stopDhcp()
{
    disconnect(database, &DhcpDatabase::leaseRemoved, this, &DhcpServer::restoreIpToPool);
    disconnect(database, &DhcpDatabase::leasedIp, this, &DhcpServer::removeIpFromPool);
    database->closeDatabase();
    delete database;

    dhcpSocket->close();
    disconnect(dhcpSocket, &QUdpSocket::readyRead, this, &DhcpServer::dhcpRecieve);
    networkList.clear();
    delete dhcpSocket;

    leaseTimer->stop();
    delete leaseTimer;

    qInfo() << "Tiat DHCP Stopped";
}
