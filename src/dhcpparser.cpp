/***************************************************
 * Copyright (C) 2018, 2019 Daniel Funk.
 *
 * This file is part of Tiat.
 * Tiat is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
****************************************************/

#include "dhcpparser.h"

DhcpParser::DhcpParser(const QByteArray &dhcpPacket, QObject *parent)
    : QObject(parent)
{
    //Strips down recieveing dhcp packet and split its values into single QByteArrays.
    op = dhcpPacket.mid(0, 1);
    hType = dhcpPacket.mid(1, 1);
    hLen = dhcpPacket.mid(2, 1);
    hops = dhcpPacket.mid(3, 1);
    xId = dhcpPacket.mid(4, 4);
    secs = dhcpPacket.mid(8, 2);
    flags = dhcpPacket.mid(10, 2);
    ciaddr = dhcpPacket.mid(12, 4);
    yiaddr = dhcpPacket.mid(16, 4);
    siaddr = dhcpPacket.mid(20, 4);
    giaddr = dhcpPacket.mid(24, 4);
    chaddr = dhcpPacket.mid(28, 6);
    sname = dhcpPacket.mid(44, 64);
    bootFile = dhcpPacket.mid(108, 128);
    magicCookie = dhcpPacket.mid(236, 4);
    dhcpOptions = dhcpPacket.mid(240);
    circuitId = getCircuitID();
    remoteId = getRemoteID();
    vendorId = getOptionData(60);
    messageType = dhcpPacket.mid(242, 1);
}

QByteArray DhcpParser::getOptionData(const quint8 &option) const
{
    QByteArray requestedOption;
    ushort length;
    for(int i = 0; i < dhcpOptions.count();) {
        //Packetsize is always at option +1, and the data is at option +2,
        length = dhcpOptions.mid(i+1, 1).toHex().toUShort(Q_NULLPTR, 16);
        if(dhcpOptions.mid(i, 1).toHex().toUShort(Q_NULLPTR, 16) == option) {
            requestedOption = dhcpOptions.mid(i+2, length);
            break;
        }

        i += length+2;
    }
    return requestedOption;
}

QByteArray DhcpParser::getCircuitID()
{
    QByteArray option82 = getOptionData(82);
    ushort packetLength = option82.mid(1, 1).toHex().toUShort(Q_NULLPTR, 16);
    QByteArray circuitID = option82.mid(2, packetLength);

    return circuitID;
}

QByteArray DhcpParser::getRemoteID()
{
    QByteArray option82 = getOptionData(82);
    //RemoteId is after the circuitId packet.
    ushort circuitIDpacketLength = option82.mid(1, 1).toHex().toUShort(Q_NULLPTR, 16);
    ushort remoteIDpacketLength = option82.mid(circuitIDpacketLength+3, 1).toHex().toUShort(Q_NULLPTR, 16);
    QByteArray remoteID = option82.mid(circuitIDpacketLength+4, remoteIDpacketLength);

    return remoteID;
}
